# Supermarket Sales

Logica de negocio del proyecto. Aqui se ejecutan las ventas y se procesan los descuentos correspondientes.

### API ###

POST /api/sale -- Crea una nueva venta.
POST /api/price -- Calcula el precio de una venta, no ejecuta los cambios.

#### Ejemplos ###

```js

POST /api/sale

Headers:

- Authorization Bearer {token}

Body:

{
	"item" : 
		{
		"product" : 
		{
				"id" : 1,
				"productType" : {
					"id" : 1,
					"initials" : "VEG",
					"description" : "Vegetales"
				},
				"name" : "Bananas",
				"salePrice" : 100,
				"costPrice" : 90,
				"stock" : 1
		},
		"quantity" : 1
		}
}

Response:

{
    "employee": null,
    "coupon": null,
    "items": [
        {
            "product": {
                "id": 1,
                "name": "Bananas",
                "costPrice": 90,
                "salePrice": 100,
                "productType": {
                    "id": 1,
                    "initials": "VEG",
                    "description": "Vegetales"
                },
                "stock": 0
            },
            "quantity": 1,
            "price": 99.2,
            "discount": 0.7999999999999972
        }
    ]
}
```

    Swagger: http://localhost:8080/api/swagger.json


