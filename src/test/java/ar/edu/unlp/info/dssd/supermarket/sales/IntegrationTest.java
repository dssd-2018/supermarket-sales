package ar.edu.unlp.info.dssd.supermarket.sales;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import ar.edu.unlp.info.dssd.supermarket.sales.domain.PriceBundle;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.Product;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.ProductType;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.Sale;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.SaleForm;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.SaleItemForm;
import ar.edu.unlp.info.dssd.supermarket.sales.services.SaleServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class IntegrationTest {

	@LocalServerPort
	protected int port;

	@Autowired
	SaleServiceImpl saleService;

	@Test
	public void testPostNewSale() {
		ProductType productType = new ProductType();
		productType.setDescription("Electro");
		productType.setId(1L);
		productType.setInitials("ELE");

		Product product = new Product();
		product.setProductType(productType);
		product.setName("MacBook Air 13.3");
		product.setCostPrice(100);
		product.setSalePrice(120);
		product.setId(1L);
		product.setStock(5);

		SaleItemForm item = new SaleItemForm();
		item.product = product;
		item.quantity = 1;

		SaleForm form = new SaleForm();
		form.item = item;

		Sale process = saleService.process(form);
		Assert.assertTrue(process.getItems().get(0).getPrice() == 110D);
	}
	
	@Test
	public void testQuotePrice() {
		ProductType productType = new ProductType();
		productType.setDescription("Casa");
		productType.setId(1L);
		productType.setInitials("CAS");

		Product product = new Product();
		product.setProductType(productType);
		product.setName("Silla");
		product.setCostPrice(3500);
		product.setSalePrice(5000);
		product.setId(1L);
		product.setStock(2);

		SaleItemForm item = new SaleItemForm();
		item.product = product;
		item.quantity = 2;

		SaleForm form = new SaleForm();
		form.item = item;

		PriceBundle process = saleService.calculatePrice(form);
		System.out.println(process.getFullPrice());
		System.out.println(process.getTotal());
		System.out.println(process.getDiscount());
		Assert.assertTrue(process.getTotal() == 8160D);
		Assert.assertTrue(process.getFullPrice() == 10000D);
		Assert.assertTrue(product.getStock() == 2);
	}

	@Test
	public void testDisplayProduct() {
		ProductType productType = new ProductType();
		productType.setDescription("Electro");
		productType.setId(1L);
		productType.setInitials("ELE");

		Product product = new Product();
		product.setProductType(productType);
		product.setName("MacBook Air 13.3");
		product.setCostPrice(100);
		product.setSalePrice(120);
		product.setId(1L);
		product.setStock(5);

		SaleItemForm item = new SaleItemForm();
		item.product = product;
		item.quantity = 1;

		SaleForm form = new SaleForm();
		form.item = item;

		List<Product> process = saleService.updateProductPrice(Arrays.asList(product));
		Assert.assertTrue(process.get(0).getSalePrice() == 120D);
		Assert.assertTrue(process.get(0).getCostPrice() == 110D);
	}
}
