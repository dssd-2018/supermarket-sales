package ar.edu.unlp.info.dssd.supermarket.sales;

import java.util.Calendar;

import org.junit.Assert;
import org.junit.Test;

import ar.edu.unlp.info.dssd.supermarket.sales.domain.Coupon;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.CouponDate;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.Employee;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.EmployeeType;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.OngoingSale;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.PriceBundle;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.Product;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.ProductType;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.Sale;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.SaleForm;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.SaleItemForm;

public class SaleTest {

	@Test
	public void testSaleStandardMargin() {
		ProductType productType = new ProductType();
		productType.setDescription("Almacen");
		productType.setId(1L);
		productType.setInitials("ALM");

		Product product = new Product();
		product.setProductType(productType);
		product.setName("Galletitas Marolio x 500gr");
		product.setCostPrice(100);
		product.setSalePrice(105);
		product.setId(1L);
		product.setStock(5);

		SaleItemForm item = new SaleItemForm();
		item.product = product;
		item.quantity = 2;

		SaleForm form = new SaleForm();
		form.item = item;

		long shouldStock = product.getStock() - item.quantity;

		Sale sale = new OngoingSale(form).sale();
		Assert.assertTrue(sale.getItems().size() > 0);

		Assert.assertTrue(sale.getItems().get(0).getPrice() == 210D);
		Assert.assertTrue(
				sale.getItems().get(0).getProduct().getStock() == shouldStock);

	}

	@Test
	public void testSaleLowMargin() {
		ProductType productType = new ProductType();
		productType.setDescription("Golosinas");
		productType.setId(1L);
		productType.setInitials("ALM");

		Product product = new Product();
		product.setProductType(productType);
		product.setName("Caramelos x 100gr");
		product.setCostPrice(0.20D);
		product.setSalePrice(1.25D);
		product.setId(1L);
		product.setStock(5);

		SaleItemForm item = new SaleItemForm();
		item.product = product;
		item.quantity = 2;

		SaleForm form = new SaleForm();
		form.item = item;

		long shouldStock = product.getStock() - item.quantity;

		Sale sale = new OngoingSale(form).sale();
		Assert.assertTrue(sale.getItems().size() > 0);
		
		System.out.println(sale.getItems().get(0).getPrice());
		
		Assert.assertTrue(sale.getItems().get(0).getPrice() == 0.86D);
		Assert.assertTrue(
				sale.getItems().get(0).getProduct().getStock() == shouldStock);

	}

	@Test
	public void testNoStock() {
		ProductType productType = new ProductType();
		productType.setDescription("Almacen");
		productType.setId(1L);
		productType.setInitials("ALM");

		Product product = new Product();
		product.setProductType(productType);
		product.setName("Galletitas Marolio x 500gr");
		product.setCostPrice(100);
		product.setSalePrice(105);
		product.setId(1L);
		product.setStock(1);

		SaleItemForm item = new SaleItemForm();
		item.product = product;
		item.quantity = 2;

		SaleForm form = new SaleForm();
		form.item = item;

		try {
			Sale sale = new OngoingSale(form).sale();
			Assert.fail();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	@Test
	public void testCouponAndEmployee() {
		ProductType productType = new ProductType();
		productType.setDescription("Almacen");
		productType.setId(1L);
		productType.setInitials("ALM");

		Product product = new Product();
		product.setProductType(productType);
		product.setName("Galletitas Marolio x 500gr");
		product.setCostPrice(100);
		product.setSalePrice(105);
		product.setId(1L);
		product.setStock(3);

		Coupon coupon = new Coupon();
		coupon.setNumber(1234L);
		coupon.setUsed(false);

		Employee employee = new Employee();
		employee.setEmployeeType(new EmployeeType());
		employee.setFirstname("abc");
		employee.setSurname("abc");
		employee.setEmail("d@d.com");

		SaleItemForm item = new SaleItemForm();
		item.product = product;
		item.quantity = 2;

		SaleForm form = new SaleForm();
		form.item = item;
		form.employee = employee;
		form.coupon = coupon;

		try {
			Sale sale = new OngoingSale(form).sale();
			Assert.fail();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	@Test
	public void testSaleStandardMarginExceeded() {
		ProductType productType = new ProductType();
		productType.setDescription("Ferreteria");
		productType.setId(1L);
		productType.setInitials("FER");

		Product product = new Product();
		product.setProductType(productType);
		product.setName("Extensor electrico");
		product.setCostPrice(100);
		product.setSalePrice(120);
		product.setId(1L);
		product.setStock(5);

		SaleItemForm item = new SaleItemForm();
		item.product = product;
		item.quantity = 2;

		SaleForm form = new SaleForm();
		form.item = item;

		Sale sale = new OngoingSale(form).sale();
		Assert.assertTrue(sale.getItems().size() > 0);

		Assert.assertTrue(sale.getItems().get(0).getPrice() == 224D);

	}

	@Test
	public void testSaleProductTypeMargin() {
		ProductType productType = new ProductType();
		productType.setDescription("Electro");
		productType.setId(1L);
		productType.setInitials("ELE");

		Product product = new Product();
		product.setProductType(productType);
		product.setName("MacBook Air 13.3");
		product.setCostPrice(100);
		product.setSalePrice(120);
		product.setId(1L);
		product.setStock(5);

		SaleItemForm item = new SaleItemForm();
		item.product = product;
		item.quantity = 2;

		SaleForm form = new SaleForm();
		form.item = item;

		Sale sale = new OngoingSale(form).sale();
		Assert.assertTrue(sale.getItems().size() > 0);

		Assert.assertTrue(sale.getItems().get(0).getPrice() == 220D);

	}

	@Test
	public void testSaleForEmployee() {

		EmployeeType empType = new EmployeeType();
		empType.setDescription("Tecnologia");
		empType.setInitials("IT");
		empType.setId("abc-123-abc");

		Employee employee = new Employee();
		employee.setEmployeeType(empType);
		employee.setEmail("carlos@gmail.com");
		employee.setFirstname("Carlos");
		employee.setSurname("Quintana");

		ProductType productType = new ProductType();
		productType.setDescription("Electro");
		productType.setId(1L);
		productType.setInitials("ELE");

		Product product = new Product();
		product.setProductType(productType);
		product.setName("MacBook Air 13.3");
		product.setCostPrice(100);
		product.setSalePrice(120);
		product.setId(1L);
		product.setStock(5);

		SaleItemForm item = new SaleItemForm();
		item.product = product;
		item.quantity = 2;

		SaleForm form = new SaleForm();
		form.item = item;
		form.employee = employee;

		Sale sale = new OngoingSale(form).sale();

		Assert.assertTrue(sale.getItems().size() > 0);
		Assert.assertTrue(sale.getItems().get(0).getPrice() == 200D);
	}

	@Test
	public void testSaleStandardMarginWithCoupon() {

		Coupon cup = new Coupon();
		cup.setId(1L);
		cup.setNumber(123L);
		cup.setUsed(false);

		ProductType productType = new ProductType();
		productType.setDescription("Electro");
		productType.setId(1L);
		productType.setInitials("ELE");

		Product product = new Product();
		product.setProductType(productType);
		product.setName("MacBook Air 13.3");
		product.setCostPrice(100);
		product.setSalePrice(120);
		product.setId(1L);
		product.setStock(5);

		SaleItemForm item = new SaleItemForm();
		item.product = product;
		item.quantity = 1;

		SaleForm form = new SaleForm();
		form.item = item;
		form.coupon = cup;

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, -1);

		CouponDate date = new CouponDate();
		date.id = 1L;
		date.from = cal.getTime();

		cal.set(Calendar.DATE, 3);
		date.to = cal.getTime();
		date.discountPercent = 0.03D;

		form.dates.add(date);

		Sale sale = new OngoingSale(form).sale();

		Assert.assertTrue(sale.getItems().size() > 0);
		Assert.assertTrue(sale.getItems().get(0).getPrice() == 106.7D);
		Assert.assertTrue(sale.getCoupon().isUsed());
	}

	@Test
	public void testDisplaySaleCatalog() {
		ProductType productType = new ProductType();
		productType.setDescription("Electro");
		productType.setId(1L);
		productType.setInitials("ELE");

		Product product = new Product();
		product.setProductType(productType);
		product.setName("MacBook Air 13.3");
		product.setCostPrice(400);
		product.setSalePrice(1000);
		product.setId(1L);
		product.setStock(5);

		SaleItemForm item = new SaleItemForm();
		item.product = product;
		item.quantity = 1;

		SaleForm form = new SaleForm();
		form.item = item;

		PriceBundle bundle = new OngoingSale(form).calculateDisplayPrice();

		Assert.assertTrue(bundle.getTotal() == 700D);
	}

	@Test
	public void testPriceWithCoupon() {
		SaleForm saleForm = new SaleForm();

		saleForm.coupon = new Coupon();
		saleForm.coupon.setNumber(506L);
		saleForm.coupon.setUsed(false);
		saleForm.coupon.setId(1L);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, -1);

		CouponDate date = new CouponDate();
		date.id = 1L;
		date.from = cal.getTime();

		cal.set(Calendar.DATE, 3);
		date.to = cal.getTime();
		date.discountPercent = 0.03D;

		saleForm.item = new SaleItemForm();

		saleForm.item.quantity = 3;

		saleForm.item.product = new Product();
		saleForm.item.product.setId(1L);
		saleForm.item.product.setName("dulce de leche sancor");
		saleForm.item.product.setCostPrice(400);
		saleForm.item.product.setSalePrice(1000);
		saleForm.item.product.setStock(48);

		ProductType productType = new ProductType();
		productType.setId(1L);
		productType.setInitials("LAC");
		productType.setDescription("Lacteo");
		saleForm.item.product.setProductType(productType);
		saleForm.dates.add(date);

		PriceBundle bundle = new OngoingSale(saleForm).calculateFinalPrice();

		System.out.print(bundle.getTotal());
		Assert.assertTrue(bundle.getTotal() == 1606.32D);

	}

	@Test
	public void testCouponAlreadyUsed() {
		SaleForm saleForm = new SaleForm();

		saleForm.coupon = new Coupon();
		saleForm.coupon.setNumber(506L);
		saleForm.coupon.setUsed(true);
		saleForm.coupon.setId(1L);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, -1);

		CouponDate date = new CouponDate();
		date.id = 1L;
		date.from = cal.getTime();

		cal.set(Calendar.DATE, 3);
		date.to = cal.getTime();
		date.discountPercent = 0.03D;

		saleForm.item = new SaleItemForm();

		saleForm.item.quantity = 3;

		saleForm.item.product = new Product();
		saleForm.item.product.setId(1L);
		saleForm.item.product.setName("dulce de leche sancor");
		saleForm.item.product.setCostPrice(400);
		saleForm.item.product.setSalePrice(1000);
		saleForm.item.product.setStock(48);

		ProductType productType = new ProductType();
		productType.setId(1L);
		productType.setInitials("LAC");
		productType.setDescription("Lacteo");
		saleForm.item.product.setProductType(productType);
		saleForm.dates.add(date);

		try {
			PriceBundle bundle = new OngoingSale(saleForm)
					.calculateFinalPrice();
			Assert.fail();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
