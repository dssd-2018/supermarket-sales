package ar.edu.unlp.info.dssd.supermarket.sales;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("ar.edu.unlp.info.dssd.supermarket.sales")
public class Application extends SpringBootServletInitializer {

	public static final String DOMAIN_PACKAGE = "ar.edu.unlp.info.dssd.supermarket.sales.domain";

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}
}
