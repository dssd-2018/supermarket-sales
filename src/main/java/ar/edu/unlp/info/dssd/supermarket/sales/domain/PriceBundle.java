package ar.edu.unlp.info.dssd.supermarket.sales.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PriceBundle {

	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	public PriceBundle() {
	}

	public PriceBundle(double total, double discountTotal) {
		this.total = round(total, 2);
		this.discount = round(discountTotal, 2);
	}

	private double total = Double.MAX_VALUE;

	private double discount = 0D;

	/**
	 * Agrega un descuento si es necesario
	 * 
	 * @param priceBundle
	 * @return
	 */
	public PriceBundle addCalculation(PriceBundle priceBundle) {
		this.total = Math.min(this.total, priceBundle.total);
		this.discount = Math.max(this.discount, priceBundle.discount);
		return this;
	}

	public PriceBundle quantity(int qty) {
		this.total = round(total * qty, 2);
		this.discount = round(discount * qty, 2);
		return this;
	}
	
	public double getTotal() {
		return this.total;
	}

	public double getDiscount() {
		return this.discount;
	}

	/**
	 * El precio SIN NINGUN descuento.
	 * @return
	 */
	public double getFullPrice() {
		return this.total + this.discount;
	}

}
