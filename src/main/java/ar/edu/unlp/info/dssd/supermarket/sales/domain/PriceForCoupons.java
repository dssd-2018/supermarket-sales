package ar.edu.unlp.info.dssd.supermarket.sales.domain;

import javax.ws.rs.WebApplicationException;

public class PriceForCoupons extends PriceCalculatorStrategy {

	@Override
	public PriceBundle calculate(PriceCalculator priceCalculator) {
		if (!priceCalculator.getSale().hasCoupon())
			// do nothing
			return priceCalculator.getPriceBundle();

		if (!new CouponsAreAllowed().isValid(priceCalculator))
			throw new WebApplicationException(
					"Los cupones no se encuentran habilitados.", 409);

		if (!new CouponWithEmployee().isValid(priceCalculator))
			throw new WebApplicationException(
					"El cupon no puede ser utilizado por empleados", 409);

		if (!new CouponUsedOnce().isValid(priceCalculator))
			throw new WebApplicationException("El cupon ya fue utilizado", 409);

		double discountPercent = priceCalculator.getSale().getCouponDates()
				.get(0).discountPercent;
		double salePrice = priceCalculator.getPriceBundle().getTotal();
		// FIXME: el valor del 3% puede variar.
		double discount = priceCalculator.getPriceBundle().getDiscount()
				+ (salePrice * discountPercent);
		double totalSalePrice = salePrice - (salePrice * discountPercent);

		return priceCalculator.getPriceBundle()
				.addCalculation(new PriceBundle(totalSalePrice, discount));
	}

}
