package ar.edu.unlp.info.dssd.supermarket.sales.domain;

public class SaleItem {

	public static class Builder extends AbstractBuilder<SaleItem> {

		public Builder() {
			this.setInstance(new SaleItem());
		}

		public Builder withProduct(Product aProduct, int quantity) {
			this.instance.product = aProduct;
			this.instance.quantity = quantity;
			return this;
		}

		public Builder price(double price) {
			this.instance.price = price;
			return this;
		}

		public Builder discount(double discount) {
			this.instance.discount = discount;
			return this;
		}

	}

	private Product product;

	private int quantity = 1;

	private double price = 0D;

	private double discount = 0D;

	public int getQuantity() {
		return quantity;
	}

	public double getPrice() {
		return price;
	}

	public double getDiscount() {
		return discount;
	}

	public Product getProduct() {
		return product;
	}

}
