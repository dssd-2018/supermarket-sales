package ar.edu.unlp.info.dssd.supermarket.sales.domain;

public class Employee {

	private EmployeeType type;
	
	private String firstname;
	
	private String surname;
	
	private String email;
	
	public EmployeeType getEmployeeType() {
		return type;
	}

	public void setEmployeeType(EmployeeType employeeType) {
		this.type = employeeType;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
