package ar.edu.unlp.info.dssd.supermarket.sales.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CouponDate {

	public Long id;
	@JsonProperty(required = true)
	public Date from;
	@JsonProperty(required = true)
	public Date to;
	@JsonProperty(required = true)
	public Double discountPercent;
	
}
