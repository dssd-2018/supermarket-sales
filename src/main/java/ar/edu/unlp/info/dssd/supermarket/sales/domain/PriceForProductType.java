package ar.edu.unlp.info.dssd.supermarket.sales.domain;

public class PriceForProductType extends PriceCalculatorStrategy {

	@Override
	public PriceBundle calculate(PriceCalculator priceCalculator) {
		if (!new SpecialProductType().isValid(priceCalculator))
			return priceCalculator.getPriceBundle();

		double salePrice = priceCalculator.getSale().getProduct()
				.getSalePrice();
		double costPrice = priceCalculator.getSale().getProduct()
				.getCostPrice();
		double totalSalePrice = ((salePrice - costPrice) * 0.5) + costPrice;
		double discount = salePrice - totalSalePrice;
		return priceCalculator.getPriceBundle().addCalculation(
				new PriceBundle(totalSalePrice, discount));
	}

}
