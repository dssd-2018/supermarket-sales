package ar.edu.unlp.info.dssd.supermarket.sales.api;

public class ErrorResponse {

	public String message = "";
	
	public ErrorResponse(String message) {
		this.message = message;
	}
	
}
