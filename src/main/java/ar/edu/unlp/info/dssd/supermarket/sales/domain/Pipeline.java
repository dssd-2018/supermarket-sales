package ar.edu.unlp.info.dssd.supermarket.sales.domain;

import java.util.ArrayList;
import java.util.List;

public abstract class Pipeline<T> {

	List<T> segments = new ArrayList<T>();
	
	public Pipeline<T> pipe(T segment) {
		this.segments.add(segment);
		return this;
	}
	
	public abstract void pipeCommand(T payload);
	
	public void process() {
		for (T object : segments) {
			this.pipeCommand(object);
		}
	}
	
}
