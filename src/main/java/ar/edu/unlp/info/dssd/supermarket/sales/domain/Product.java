package ar.edu.unlp.info.dssd.supermarket.sales.domain;

import javax.ws.rs.WebApplicationException;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Product {

	private Long id;
	@JsonProperty(required = true)
	private String name;
	@JsonProperty(required = true)
	private double costPrice;
	@JsonProperty(required = true)
	private double salePrice;
	@JsonProperty(required = true)
	private ProductType productType;
	@JsonProperty(required = true)
	private long stock;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(double costPrice) {
		this.costPrice = costPrice;
	}

	public double getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(double salePrice) {
		this.salePrice = salePrice;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double margin() {
		return (this.getSalePrice() - this.getCostPrice());
	}

	public double marginPercent() {
		return ((this.getSalePrice() - this.getCostPrice())
				/ this.getCostPrice());
	}

	public boolean hasProductType(String string) {
		return this.getProductType().getDescription().equals("Electro");
	}

	public long getStock() {
		return stock;
	}

	public void updateStock(long qty) {
		if (this.stock - qty < 0)
			throw new WebApplicationException(
					"No hay suficiente stock para satisfacer la demanda del producto "
							+ this.getId(),
					409);
		this.stock = this.stock - qty;
	}

	public void setStock(long stock) {
		this.stock = stock;
	}

}
