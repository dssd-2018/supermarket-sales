package ar.edu.unlp.info.dssd.supermarket.sales.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Coupon {

	private Long id;
	@JsonProperty(required = true)
	private Long number;
	@JsonProperty(required = true)
	private boolean used = true;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNumber() {
		return number;
	}

	public void setNumber(Long number) {
		this.number = number;
	}

	public boolean isUsed() {
		return used;
	}

	public void setUsed(boolean used) {
		this.used = used;
	}
	
	
	
}
