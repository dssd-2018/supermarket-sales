package ar.edu.unlp.info.dssd.supermarket.sales.domain;

public interface PriceCalculationRule {

	boolean isValid(PriceCalculator priceCalculator);
	
}
