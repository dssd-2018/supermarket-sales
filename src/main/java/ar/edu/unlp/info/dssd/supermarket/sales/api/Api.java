package ar.edu.unlp.info.dssd.supermarket.sales.api;

import java.net.URISyntaxException;
import java.util.List;
import java.util.List;
import java.util.List;

import javax.inject.Singleton;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import ar.edu.unlp.info.dssd.supermarket.sales.domain.Product;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.SaleForm;
import ar.edu.unlp.info.dssd.supermarket.sales.services.ApiInfoServiceImpl;
import ar.edu.unlp.info.dssd.supermarket.sales.services.AuthServiceImpl;
import ar.edu.unlp.info.dssd.supermarket.sales.services.SaleServiceImpl;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Component
@Singleton
@Path("/")
@io.swagger.annotations.Api
public class Api {

	Logger logger = LoggerFactory.getLogger(Api.class);
	
	@Autowired
	ApiInfoServiceImpl apiInfo;
	@Autowired
	AuthServiceImpl authService;
	@Autowired
	SaleServiceImpl saleService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Api Status", tags = {"info"})
	public Response ping() {
		try {
			return Response.ok(apiInfo.getInfo()).build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getMessage()).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("sale")
	@ApiOperation(value = "Crear una nueva venta", tags = {"Venta"})
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Venta generada."),
			@ApiResponse(code = 400, response = String.class, message = "Parametros no validos."),
			@ApiResponse(code = 409, response = String.class, message = "Regla de negocio violada.")})
	public Response create(SaleForm form, @Context HttpHeaders headers)
			throws URISyntaxException {
		this.debug(form);
		this.checkAuth(headers);
		return Response.ok(saleService.process(form)).build();
	}

	private void debug(Object...objects) {
		logger.info(new Gson().toJson(objects));
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("price")
	@ApiOperation(value = "Devuelve los valores esperados para esa venta.", tags = {"Venta"})
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Precios relativos a la venta."),
			@ApiResponse(code = 400, response = String.class, message = "Parametros no validos."),
			@ApiResponse(code = 409, response = String.class, message = "Regla de negocio violada.")})
	public Response calculatePrice(SaleForm form, @Context HttpHeaders headers)
			throws URISyntaxException {
		this.debug(form);
		this.checkAuth(headers);
		return Response.ok(saleService.calculatePrice(form)).build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("price")
	@ApiOperation(value = "Devuelve los valores esperados para esa venta.", tags = {"Venta"})
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Productos ingresados con precios actualizados."),
			@ApiResponse(code = 400, response = String.class, message = "Parametros no validos."),
			@ApiResponse(code = 409, response = String.class, message = "Regla de negocio violada.")})
	public Response updateProductPrice(List<Product> products, @Context HttpHeaders headers)
			throws URISyntaxException {
		this.debug(products);
		this.checkAuth(headers);
		return Response.ok(saleService.updateProductPrice(products)).build();
	}

	private void checkAuth(HttpHeaders headers) {
		String headerString = headers.getHeaderString("Authorization");
		if (headerString == null || headerString.isEmpty())
			throw new BadRequestException("Token is missing");

		authService.auth(headerString);
	}

}
