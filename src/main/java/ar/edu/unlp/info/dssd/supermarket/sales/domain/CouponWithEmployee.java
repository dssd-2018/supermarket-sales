package ar.edu.unlp.info.dssd.supermarket.sales.domain;

public class CouponWithEmployee implements PriceCalculationRule {

	@Override
	public boolean isValid(PriceCalculator priceCalculator) {
		return !priceCalculator.getSale().hasEmployee()
				|| !priceCalculator.getSale().hasCoupon();
	}

}
