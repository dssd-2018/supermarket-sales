package ar.edu.unlp.info.dssd.supermarket.sales.domain;

import javax.ws.rs.WebApplicationException;

public class PriceForEmployees extends PriceCalculatorStrategy {

	@Override
	public PriceBundle calculate(PriceCalculator priceCalculator) {

		if (!priceCalculator.getSale().hasEmployee())
			return priceCalculator.getPriceBundle();

		if (!new CouponWithEmployee().isValid(priceCalculator))
			throw new WebApplicationException(
					"Si hay empleado no puede ingresar cupones", 409);

		return priceCalculator.getPriceBundle().addCalculation(
				new PriceBundle(
						priceCalculator.getSale().getProduct().getCostPrice(),
						priceCalculator.getSale().getProduct().getSalePrice()
								- priceCalculator.getSale().getProduct()
										.getCostPrice()));
	}

}
