package ar.edu.unlp.info.dssd.supermarket.sales.domain;

public class PriceCalculator {

	public static class Builder extends AbstractBuilder<PriceCalculator> {

		public Builder() {
			this.instance = new PriceCalculator();
		}

		public Builder strategies(PriceCalculatorStrategy... strategies) {
			for (PriceCalculatorStrategy aStrategy : strategies) {
				this.instance.pipeline.pipe(aStrategy);
			}
			return this;
		}

		public Builder sale(OngoingSale aSale) {
			this.instance.ongoingSale = aSale;
			return this;
		}

	}

	private OngoingSale ongoingSale;

	private PriceBundle priceBundle = new PriceBundle();

	private Pipeline<PriceCalculatorStrategy> pipeline = new Pipeline<PriceCalculatorStrategy>() {

		@Override
		public void pipeCommand(PriceCalculatorStrategy payload) {
			payload.calculate(PriceCalculator.this);
		}
	};

	public OngoingSale getSale() {
		return ongoingSale;
	}

	public PriceBundle getPriceBundle() {
		return priceBundle;
	}

	public PriceBundle calculate() {
		if (ongoingSale == null)
			throw new IllegalArgumentException("No sale defined");
		pipeline.process();
		return this.getPriceBundle().quantity(this.getSale().getQuantity());
	}

}
