package ar.edu.unlp.info.dssd.supermarket.sales.domain;

public abstract class PriceCalculatorStrategy {

	public abstract PriceBundle calculate(PriceCalculator priceCalculator);

}
