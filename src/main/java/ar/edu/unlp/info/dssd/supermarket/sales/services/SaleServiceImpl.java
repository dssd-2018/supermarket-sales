package ar.edu.unlp.info.dssd.supermarket.sales.services;

import java.util.List;

import javax.ws.rs.BadRequestException;

import org.springframework.stereotype.Service;

import ar.edu.unlp.info.dssd.supermarket.sales.domain.OngoingSale;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.PriceBundle;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.Product;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.Sale;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.SaleForm;
import ar.edu.unlp.info.dssd.supermarket.sales.domain.SaleItemForm;

@Service
public class SaleServiceImpl {

	public Sale process(SaleForm form) {
		if (form == null)
			throw new BadRequestException();
		
		this.validate(form);
		
		return new OngoingSale(form).sale();
	}

	private void validate(SaleForm form) {
		if (form.item == null)
			throw new BadRequestException("No se ha ingresado ningun item.");
		
		if (form.item.product == null)
			throw new BadRequestException("No se ha ingresado ningun producto");
		
		if (form.item.product.getProductType() == null)
			throw new BadRequestException("No se ha ingresado ningun tipo de producto");
		
		if (form.item.product.getName() == null)
			throw new BadRequestException("El producto no tiene nombre asociado");
		
		if (form.item.product.getProductType().getDescription() == null)
			throw new BadRequestException("El tipo de producto no tiene descripcion");
	}

	public PriceBundle calculatePrice(SaleForm form) {
		if (form == null)
			throw new BadRequestException();
		
		this.validate(form);
		
		return new OngoingSale(form).calculateFinalPrice();
	}

	public List<Product> updateProductPrice(List<Product> products) {

		for (Product aProduct : products) {
			SaleForm saleSample = new SaleForm();
			saleSample.item = new SaleItemForm();
			saleSample.item.product = aProduct;
			saleSample.item.quantity = 1;
			PriceBundle price = new OngoingSale(saleSample).calculateDisplayPrice();
			aProduct.setCostPrice(price.getTotal());
		}

		return products;
	}

}
