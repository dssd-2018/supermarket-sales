package ar.edu.unlp.info.dssd.supermarket.sales.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import ar.edu.unlp.info.dssd.supermarket.sales.domain.ApiInfo;

@Service
@PropertySource("classpath:application.properties")
public class ApiInfoServiceImpl {

	@Value("${api.version}")
	String apiVersion = "0";

	public ApiInfo getInfo() {
		ApiInfo info = new ApiInfo();

		info.version = apiVersion;
		info.sha = System.getenv("COMMIT_SHA");
		info.deploy_timestamp = System.getenv("LAST_DEPLOY_TIMESTAMP");
		info.status = "OK";
		return info;
	}

}
