package ar.edu.unlp.info.dssd.supermarket.sales.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductType {

	private Long id;

	private String initials;
	@JsonProperty(required = true)
	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getInitials() {
		return initials;
	}

	public void setInitials(String initials) {
		this.initials = initials;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
