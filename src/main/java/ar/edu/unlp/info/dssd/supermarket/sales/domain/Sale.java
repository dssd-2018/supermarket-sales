package ar.edu.unlp.info.dssd.supermarket.sales.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Sale {

	public static class Builder extends AbstractBuilder<Sale> {
		public Builder() {
			this.instance = new Sale();
		}

		public Builder employee(Employee emp) {
			this.instance.employee = emp;
			return this;
		}

		public Builder coupon(Coupon cup) {
			this.instance.coupon = cup;
			return this;
		}

		public Builder items(SaleItem... items) {
			this.instance.items.addAll(Arrays.asList(items));
			return this;
		}
	}

	private Employee employee = null;

	private Coupon coupon = null;

	List<SaleItem> items = new ArrayList<SaleItem>();

	public Employee getEmployee() {
		return employee;
	}

	public Coupon getCoupon() {
		return coupon;
	}

	public List<SaleItem> getItems() {
		return new ArrayList<SaleItem>(this.items);
	}

}
