package ar.edu.unlp.info.dssd.supermarket.sales.api;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class ApplicationExceptionMapper implements ExceptionMapper<WebApplicationException> {

	Logger logger = LoggerFactory.getLogger(GenericExceptionMapper.class);

	@Override
	public Response toResponse(WebApplicationException arg0) {
		logger.error(arg0.getMessage(), arg0);
		return Response.status(arg0.getResponse().getStatus()).entity(new ErrorResponse(arg0.getMessage())).build();
	}

}
