package ar.edu.unlp.info.dssd.supermarket.sales.domain;

public class SpecialProductType implements PriceCalculationRule {

	@Override
	public boolean isValid(PriceCalculator priceCalculator) {
		return priceCalculator.getSale().getProduct().hasProductType("Electro");
	}

}
