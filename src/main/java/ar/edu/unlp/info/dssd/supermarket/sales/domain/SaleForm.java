package ar.edu.unlp.info.dssd.supermarket.sales.domain;

import java.util.ArrayList;
import java.util.List;

public class SaleForm {
	
	public SaleItemForm item = new SaleItemForm();
	
	public Coupon coupon;
	
	public Employee employee;
	
	public List<CouponDate> dates = new ArrayList<CouponDate>();
}
