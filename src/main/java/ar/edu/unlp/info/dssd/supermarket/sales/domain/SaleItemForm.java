package ar.edu.unlp.info.dssd.supermarket.sales.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SaleItemForm {
	
	@JsonProperty(required = true)
	public Product product = null;
	@JsonProperty(required = true)
	public int quantity = 0;
	
}
