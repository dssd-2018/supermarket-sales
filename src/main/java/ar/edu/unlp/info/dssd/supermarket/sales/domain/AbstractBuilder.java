package ar.edu.unlp.info.dssd.supermarket.sales.domain;

public class AbstractBuilder<T> {

	T instance;
	
	public void setInstance(T instance) {
		this.instance = instance;
	}
	
	public T build() {
		return instance;
	}
	
}
