package ar.edu.unlp.info.dssd.supermarket.sales.domain;

public class CouponsAreAllowed implements PriceCalculationRule {

	@Override
	public boolean isValid(PriceCalculator priceCalculator) {
		return !priceCalculator.getSale().getCouponDates().isEmpty();
	}

}
