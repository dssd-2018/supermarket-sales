package ar.edu.unlp.info.dssd.supermarket.sales.domain;

public class StandardDiscount extends PriceCalculatorStrategy {

	@Override
	public PriceBundle calculate(PriceCalculator priceCalculator) {
		// PrecioVentaFinal = ((PrecioVenta - (PrecioCosto + 10%)) * 80%) +
		// (PrecioCosto + 10%).
		if (new SpecialProductType().isValid(priceCalculator))
			return priceCalculator.getPriceBundle();
		double costPrice = priceCalculator.getSale().getProduct()
				.getCostPrice();
		double salePrice = priceCalculator.getSale().getProduct()
				.getSalePrice();
		double discount = 0D;
		double totalSalePrice = salePrice;

		if (priceCalculator.getSale().getProduct().marginPercent() > 0.1) {
			costPrice = priceCalculator.getSale().getProduct().getCostPrice();
			salePrice = priceCalculator.getSale().getProduct().getSalePrice();
			double margin = priceCalculator.getSale().getProduct().margin();
			totalSalePrice = costPrice + (((margin - (costPrice * 0.1)) * 0.2)
					+ (costPrice * 0.1));
			discount = salePrice - totalSalePrice;

		}

		return priceCalculator.getPriceBundle().addCalculation(
				new PriceBundle(totalSalePrice, discount));
	}

}
