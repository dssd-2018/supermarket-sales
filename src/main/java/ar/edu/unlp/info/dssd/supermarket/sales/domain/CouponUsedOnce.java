package ar.edu.unlp.info.dssd.supermarket.sales.domain;

public class CouponUsedOnce implements PriceCalculationRule {

	@Override
	public boolean isValid(PriceCalculator priceCalculator) {
		return priceCalculator.getSale().hasCoupon()
				&& !priceCalculator.getSale().getCoupon().isUsed();
	}

}
