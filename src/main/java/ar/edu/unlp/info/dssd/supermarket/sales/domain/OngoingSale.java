package ar.edu.unlp.info.dssd.supermarket.sales.domain;

import java.util.ArrayList;
import java.util.List;

public class OngoingSale {

	private Employee employee = null;
	private Coupon coupon = null;
	private Product product = null;
	private int quantity = 0;
	private List<CouponDate> couponDates = new ArrayList<CouponDate>();

	public OngoingSale(SaleForm input) {
		this.employee = input.employee;
		this.coupon = input.coupon;
		this.product = input.item.product;
		this.quantity = input.item.quantity;
		this.couponDates.addAll(input.dates);
	}

	public Sale sale() {
		PriceBundle total = calculateFinalPrice();
		product.updateStock(quantity);
		this.processCoupon();
		return new Sale.Builder().coupon(this.coupon).employee(this.employee)
				.items(new SaleItem.Builder()
						.withProduct(this.product, this.quantity)
						.price(total.getTotal()).discount(total.getDiscount())
						.build())
				.build();
	}

	private void processCoupon() {
		if (this.hasCoupon())
			coupon.setUsed(true);
	}

	public PriceBundle calculateFinalPrice() {
		return new PriceCalculator.Builder().sale(this)
				.strategies(new PriceForEmployees(), new StandardDiscount(),
						new PriceForProductType(), new PriceForCoupons())
				.build().calculate();
	}

	public boolean hasEmployee() {
		return this.getEmployee() != null;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Coupon getCoupon() {
		return coupon;
	}

	public void setCoupon(Coupon coupon) {
		this.coupon = coupon;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public boolean hasCoupon() {
		return this.getCoupon() != null;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public List<CouponDate> getCouponDates() {
		return this.couponDates;
	}

	public PriceBundle calculateDisplayPrice() {
		return new PriceCalculator.Builder().sale(this)
				.strategies(new StandardDiscount(), new PriceForProductType())
				.build().calculate();
	}

}
